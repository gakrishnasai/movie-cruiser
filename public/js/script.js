// All your js should reside here

var appKey = "8a3adb9409adaaf59bad06608475df8b";
var getGenre = "https://api.themoviedb.org/3/genre/movie/list?api_key=8a3adb9409adaaf59bad06608475df8b&language=en-US";
var responseTextMovie  = "https://api.themoviedb.org/3/movie/550?api_key=8a3adb9409adaaf59bad06608475df8b";
var baseImgUrl = "http://image.tmdb.org/t/p/w185"
// https://api.themoviedb.org/3/movie/top_rated?api_key=8a3adb9409adaaf59bad06608475df8b&language=en-US&page=1
var page1Movies ="https://api.themoviedb.org/3/discover/movie?api_key=8a3adb9409adaaf59bad06608475df8b";
var baseUrl = "https://api.themoviedb.org/3/discover/movie?api_key=8a3adb9409adaaf59bad06608475df8b";
var pageNo = 0;
var posterLink = "";
var responseText;
var httpRequest;


function pageLoad(){
    console.log('pageLoad');
    var url = baseUrl + "&page=1";
    loadMasterMovies(url);
    loadFavorites();
}

function loadNextMovies(){
    console.log('loadNextMovies');
    // document.getElementById('card-row').innerHTML = "";
    var url = baseUrl + "&page=" + (pageNo+1);
    loadMasterMovies(url);
    loadFavorites();
}

function loadPrevMovies(){
    console.log('loadPrevMovies');
    // document.getElementById('card-row').innerHTML = "";
    if(pageNo != 1 || pageNo != 0){
        var url = baseUrl + "&page=" + (pageNo-1);
        loadMasterMovies(url);
        loadFavorites();
    }
}

function loadMasterMovies(url){
    console.log('loadMasterMovies');
    document.getElementById('card-row').innerHTML = "";
    // document.getElementById('card-fav-row').innerHTML = "";
    
    httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    httpRequest.onreadystatechange = alterCardDiv;
    httpRequest.open('GET', url);
    httpRequest.send();
}

function alterCardDiv() {
    console.log('alterCardDiv');
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
          if (httpRequest.status === 200) {
                responseText = JSON.parse(httpRequest.responseText);
                // console.log(responseText.page);
                pageNo = responseText.page;
                var div = document.getElementById('card-row');
                for(i=0; i <responseText.results.length; i++){
                    var imgSrc = baseImgUrl + responseText.results[i].poster_path;
                    var imgAltText = responseText.results[i].title;
                    var id = responseText.results[i].id;
                    var movieOverview = responseText.results[i].overview;
                    div.innerHTML += '<div class="col-6 col-md-3"><div class="card"><img src='+ imgSrc + ' alt="' + imgAltText + '" class="img-fluid"><div class="card-block"><div class="col-md-10"><h6 class="card-title">'+ imgAltText +'<div onclick="toggle1(' + id + ', this)" id="testId-'+id+'"><i class="far fa-heart"></div></h6></div>';
                }
          } else {
                    alert('There was a problem with the request.');
        }
    }
}

function toggle1(id, x){
    // xx = document.getElementById('testId-' + id);
    if(x.innerHTML == '<i class="far fa-heart"></i>'){
        x.innerHTML='';
        x.innerHTML='<i class="fas fa-heart"></i>';
        toggleFavorites(0, id);
    }
    else{
        x.innerHTML='';
        x.innerHTML='<i class="far fa-heart"></i>';
        toggleFavorites(1, id);
    }
}

function toggleFavorites(deleted, id){ 
    let httpRequest = new XMLHttpRequest();
    let isDeleted = deleted;
    var indexId = responseText.results.findIndex(obj => obj.id === id);
    console.log(responseText.results[indexId]);
    if(isDeleted == 1){
        httpRequest.open('DELETE', 'http://localhost:3000/favorites');
        httpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        httpRequest.send(JSON.stringify(responseText.results[indexId]));
    }
    else if (isDeleted == 0) {
        httpRequest.open('POST', 'http://localhost:3000/favorites');
        httpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        httpRequest.send(JSON.stringify(responseText.results[indexId]));
    }
}

function loadFavorites() {
    var xhr = new XMLHttpRequest(),
        method = "GET",
        url = "http://localhost:3000/favorites";
    document.getElementById('card-fav-row').innerHTML="";
    xhr.open(method, url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            var div = document.getElementById('card-fav-row');
            var responseFavText = JSON.parse(xhr.responseText);
            for (i = 0; i < responseFavText.length; i++) {
                var imgSrc = baseImgUrl + responseFavText[i].poster_path;
                var imgAltText = responseFavText[i].title;
                var id = responseFavText[i].id;
                var movieOverview = responseFavText[i].overview;
                div.innerHTML += '<div class="col-6 col-md-3"><div class="card"><img src=' + imgSrc + ' alt="' + imgAltText + '" class="img-fluid"><div class="card-block"><div class="col-md-10"><h6 class="card-title">' + imgAltText + '<div onclick="toggle1(' + id + ', this)" id="testId"><i class="far fa-heart"></div></h6></div>';
            }
        }
    };
    xhr.send();
    
}

function alterFavCardDiv() {

}
